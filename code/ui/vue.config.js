module.exports = {
    devServer: {
        // proxy: {
        //     "/api/*": {
        //         target: "nginx_container",
        //         secure: false
        //     }
        // },
        disableHostCheck: true,
        // // host: 'nginx_container',
        port: 8002,
        // watchOptions: {
        //     aggregateTimeout: 500, // delay before reloading
        //     poll: 1000 // enable polling since fsevents are not supported in docker
        // }
    },
    //lintOnSave: false
}
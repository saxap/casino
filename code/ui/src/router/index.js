import Vue from 'vue'
import VueRouter from 'vue-router'
import Rolling from "../views/Rolling";
import RoundsReport from "../views/RoundsReport";
import MostActiveGamersReport from "../views/MostActiveGamersReport";

Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      name: 'Rolling',
      component: Rolling
    },
    {
      path: '/rounds-report',
      name: 'RoundsReport',
      component: RoundsReport
    },
    {
      path: '/most-active-gamers-report',
      name: 'MostActiveGamersReport',
      component: MostActiveGamersReport
    },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

<?php

namespace App\Controller;

use App\Dto\RollDto;
use App\Service\CasinoService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    protected CasinoService $casinoService;

    public function __construct(
        CasinoService $casinoService
    )
    {
        $this->casinoService = $casinoService;
    }

    /**
     * @Route("/info", name="info")
     */
    public function index()
    {
        phpinfo();
        die();
    }

    /**
     * @Route("/roll", name="roll", methods={"POST"})
     * @param RollDto $rollDto
     * @return JsonResponse
     */
    public function roll(RollDto $rollDto)
    {
        return $this->casinoService->roll($rollDto);
    }

    /**
     * @Route("/rounds-report", name="roundsReport", methods={"GET"})
     */
    public function roundsReport()
    {
        return $this->casinoService->roundsReport();
    }

    /**
     * @Route("/most-active-gamers-report", name="mostActiveGamersReport", methods={"GET"})
     */
    public function mostActiveGamersReport()
    {
        return $this->casinoService->mostActiveGamersReport();
    }
}

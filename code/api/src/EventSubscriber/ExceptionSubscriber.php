<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public function onExceptionEvent(ExceptionEvent $event)
    {
        $throwException = $event->getThrowable();

        $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;

        if ($throwException instanceof HttpExceptionInterface) {
            $statusCode = $throwException->getStatusCode();
        }

//        if ($throwException instanceof AuthenticationException) {
//            $statusCode = Response::HTTP_UNAUTHORIZED;
//        }

        $errorBody = [
            'errorCode' => $throwException->getCode(),
            'message' => $throwException->getMessage(),
        ];

        $event->setResponse(new JsonResponse(['success' => false, 'data' => $errorBody], $statusCode));

    }

    public static function getSubscribedEvents()
    {
        return [
            ExceptionEvent::class => ['onExceptionEvent', -125]
        ];
    }
}

<?php


namespace App\Service;


use App\BaseService;
use App\Dto\RollDto;
use App\Entity\Cell;
use App\Entity\Roll;
use App\Entity\Round;
use App\Repository\CellRepository;
use App\Repository\RoundRepository;
use LogicException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Contracts\Cache\ItemInterface;

class CasinoService extends BaseService
{
    const MAX_ROLLS_COUNT = 'max_rolls_count';

    private AdapterInterface $cache;

    private CellRepository $cellRepository;

    private RoundRepository $roundRepository;

    public function __construct(
        CellRepository $cellRepository,
        RoundRepository $roundRepository,
        AdapterInterface $cache
    )
    {
        $this->cellRepository = $cellRepository;
        $this->roundRepository = $roundRepository;
        $this->cache = $cache;
    }

    /**
     * @return int
     */
    private function getMaxRollsCount() : int
    {
        return $this->cache->get(self::MAX_ROLLS_COUNT, function (ItemInterface $item) {
            $item->expiresAfter(3600);
            return count($this->cellRepository->findAll());
        });
    }

    private function startNewRound(string $gamer) : Round {
        $round = new Round();
        $round->setGamer($gamer);
        $this->entityManager->persist($round);
        $this->entityManager->flush();
        return $round;
    }

    private function findOrStartNewRound(RollDto $rollDto) : Round
    {
        if ($rollDto->isNewRound()) {
            return $this->startNewRound($rollDto->getGamer());
        }

        // пробуем найти текущий раунд
        $round = $this->roundRepository->findOneBy([
            'gamer' => $rollDto->getGamer()
        ], [
            'startedAt' => 'DESC'
        ]);

        $maxRollsCount = $this->getMaxRollsCount();

        // если нету или максимальное количество роллов то создаем новый раунд
        if (!$round || $round->getRolls()->count() >= $maxRollsCount) {
            $round = $this->startNewRound($rollDto->getGamer());
        }

        return $round;
    }

    public function roll(RollDto $rollDto)
    {
        $round = $this->findOrStartNewRound($rollDto);

        $possibleCells = $this->cellRepository->getPossibleCells($round->getId());

        if (!$possibleCells) {
            throw new LogicException('No possible cells found.');
        }

        if (count($possibleCells) != 1) {

            $totalWeight = array_reduce($possibleCells, fn($total, Cell $cell) => $cell->isJackpot() ? $total : $total + $cell->getWeight());

            $rolled = rand(0, $totalWeight);
            $rolledCell = null;
            $processedWeight = 0;
            foreach ($possibleCells as $cell) {
                $processedWeight += $cell->getWeight();
                if ($rolled <= $processedWeight) {
                    $rolledCell = $cell;
                    break;
                }
            }
        } else {
            // получается тут джекпот
            $rolledCell = $possibleCells[0];
        }

        $roll = new Roll();
        $roll->setCell($rolledCell);
        $roll->setRound($round);
        $round->getRolls()->add($roll);
        $this->entityManager->persist($round);
        $this->entityManager->flush();

        return $this->response($roll, [AbstractNormalizer::IGNORED_ATTRIBUTES => ['rolls']]);
    }

    public function roundsReport()
    {
        $report = $this->roundRepository->getRoundsReport();
        return $this->response($report);
    }

    public function mostActiveGamersReport()
    {
        $report = $this->roundRepository->getMostActiveGamersReport();
        return $this->response($report);
    }
}
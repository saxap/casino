<?php


namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;


class RollDto
{
    /**
     * @var string|null
     * @Assert\NotBlank
     */
    protected ?string $gamer;

    protected ?bool $newRound;

    /**
     * @return string|null
     */
    public function getGamer(): ?string
    {
        return $this->gamer;
    }

    /**
     * @param string|null $gamer
     */
    public function setGamer(?string $gamer): void
    {
        $this->gamer = $gamer;
    }

    /**
     * @return bool|null
     */
    public function isNewRound(): bool
    {
        return $this->newRound ?? false;
    }

    /**
     * @param bool|null $newRound
     */
    public function setNewRound(?bool $newRound): void
    {
        $this->newRound = $newRound;
    }

}
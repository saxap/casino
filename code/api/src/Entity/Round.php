<?php

namespace App\Entity;

use App\Repository\RoundRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoundRepository::class)
 */
class Round
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gamer;

    /**
     * @ORM\OneToMany(targetEntity=Roll::class, mappedBy="round", orphanRemoval=true, cascade={"persist"})
     */
    private $rolls;

    public function __construct()
    {
        $this->rolls = new ArrayCollection();
        $this->startedAt = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartedAt(): ?\DateTimeInterface
    {
        return $this->startedAt;
    }

    public function getGamer(): ?string
    {
        return $this->gamer;
    }

    public function setGamer(string $gamer): self
    {
        $this->gamer = $gamer;

        return $this;
    }

    /**
     * @return Collection|Roll[]
     */
    public function getRolls(): Collection
    {
        return $this->rolls;
    }

    public function addRoll(Roll $roll): self
    {
        if (!$this->rolls->contains($roll)) {
            $this->rolls[] = $roll;
            $roll->setRound($this);
        }

        return $this;
    }

    public function removeRoll(Roll $roll): self
    {
        if ($this->rolls->contains($roll)) {
            $this->rolls->removeElement($roll);
            // set the owning side to null (unless already changed)
            if ($roll->getRound() === $this) {
                $roll->setRound(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Repository;

use App\Entity\Round;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Round|null find($id, $lockMode = null, $lockVersion = null)
 * @method Round|null findOneBy(array $criteria, array $orderBy = null)
 * @method Round[]    findAll()
 * @method Round[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoundRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Round::class);
    }

    public function getRoundsReport()
    {
        $sql = <<<SQL
            SELECT round_num, COUNT(round_num) as gamers_count
            FROM (
                SELECT COUNT(r.gamer) as round_num, r.gamer FROM round r GROUP BY r.gamer
            ) q
            GROUP BY round_num
            ORDER BY round_num DESC
        SQL;

        $rsm = new ResultSetMapping;
        $rsm->addScalarResult('round_num', 'roundNum');
        $rsm->addScalarResult('gamers_count', 'gamersCount');

        $query = $this->_em->createNativeQuery($sql, $rsm);

        return $query->getArrayResult();
    }

    public function getMostActiveGamersReport()
    {
        $sql = <<<SQL
            SELECT COUNT(r.gamer) as round_num, r.gamer, COALESCE(AVG(rolls_count), 0) as avg_rolls_count
            FROM round r
            LEFT JOIN (
                SELECT COUNT(ro.round_id) as rolls_count, ro.round_id
                FROM roll ro
                GROUP BY ro.round_id
            ) ro ON ro.round_id = r.id
            GROUP BY r.gamer
            ORDER BY round_num DESC
        SQL;

        $rsm = new ResultSetMapping;
        $rsm->addScalarResult('round_num', 'roundNum');
        $rsm->addScalarResult('gamer', 'gamer');
        $rsm->addScalarResult('avg_rolls_count', 'avgRollsCount', 'integer');

        $query = $this->_em->createNativeQuery($sql, $rsm);

        return $query->getArrayResult();
    }
}

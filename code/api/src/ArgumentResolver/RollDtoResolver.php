<?php


namespace App\ArgumentResolver;


use App\Dto\RollDto;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RollDtoResolver implements ArgumentValueResolverInterface
{
    protected SerializerInterface $serializer;

    protected ValidatorInterface $validator;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator
    )
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument)
    {
        if ($argument->getType() === RollDto::class) {
            return true;
        }

        return false;
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $data = $request->getContent();
        $dto = $this->serializer->deserialize($data, RollDto::class, 'json');
        $this->validate($dto);
        yield $dto;
    }

    private function validate($object)
    {
        $errors = $this->validator->validate($object);

        if (count($errors) > 0) {
            $errorsString = [];
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $errorsString[] = $error->getPropertyPath().' - '.$error->getMessage();
            }

            throw new BadRequestHttpException(implode("\n", $errorsString));
        }
    }

}
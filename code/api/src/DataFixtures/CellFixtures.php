<?php

namespace App\DataFixtures;

use App\Entity\Cell;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CellFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $weights = [20, 100, 45, 70, 15, 140, 20, 20, 140, 45];

        foreach ($weights as $weight) {
            $cell = new Cell();
            $cell->setWeight($weight);
            $cell->setJackpot(false);
            $manager->persist($cell);
        }

        $cell = new Cell();
        $cell->setWeight(0);
        $cell->setJackpot(true);
        $manager->persist($cell);

        $manager->flush();
    }
}

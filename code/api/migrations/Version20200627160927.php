<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200627160927 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE cell_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE roll_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE round_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE cell (id INT NOT NULL, weight INT NOT NULL, jackpot BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE roll (id INT NOT NULL, cell_id INT NOT NULL, round_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2EB532CECB39D93A ON roll (cell_id)');
        $this->addSql('CREATE INDEX IDX_2EB532CEA6005CA0 ON roll (round_id)');
        $this->addSql('CREATE TABLE round (id INT NOT NULL, started_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, gamer VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE roll ADD CONSTRAINT FK_2EB532CECB39D93A FOREIGN KEY (cell_id) REFERENCES cell (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE roll ADD CONSTRAINT FK_2EB532CEA6005CA0 FOREIGN KEY (round_id) REFERENCES round (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE roll DROP CONSTRAINT FK_2EB532CECB39D93A');
        $this->addSql('ALTER TABLE roll DROP CONSTRAINT FK_2EB532CEA6005CA0');
        $this->addSql('DROP SEQUENCE cell_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE roll_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE round_id_seq CASCADE');
        $this->addSql('DROP TABLE cell');
        $this->addSql('DROP TABLE roll');
        $this->addSql('DROP TABLE round');
    }
}

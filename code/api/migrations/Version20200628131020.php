<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200628131020 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $weights = [20, 100, 45, 70, 15, 140, 20, 20, 140, 45];
        foreach ($weights as $weight) {
            $this->addSql('INSERT INTO cell (id, weight, jackpot) VALUES (nextval(\'cell_id_seq\'), '.$weight.', false)');
        }
        $this->addSql('INSERT INTO cell (id, weight, jackpot) VALUES (nextval(\'cell_id_seq\'), 0, true)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DELETE FROM cell');
    }
}
